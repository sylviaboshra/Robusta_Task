describe('User page', () => {
  // fixture data file for parents data
    beforeEach(function () {
      cy.fixture('parentdata').then((user) => {
        this.user = user
      })
    })
  
    it('has user', function () {

        //visit ta3limy website
        cy.visit("https://www.ta3limy.com/")

        //click on register button
        cy.get('a[href*="register"]').first().click()

        //choose register as parent
        cy.get('[for="parent"]').click()

        //insert the first name
        cy.get('[name="firstName"]').type(this.user.firstname)

        //insert the last name
        cy.get('[name="lastName"]').type(this.user.lastname)

        //insert the mobile number
        cy.get('[name="mobileNumber"]').type(this.user.mobile)

        //insert the password
        cy.get('[name="password"]').type(this.user.password)

        //confirm password
        cy.get('[name="passwordConfirmation"]').type(this.user.password)



        //choose gender
        if(this.user.gender = "female") {
          cy.get('[for="female"]').click()
        }
        else {
          cy.get('[for="male"]').click()
        }

        //wait for 1 second
        cy.wait(1000);


        //choose the age range

        if(this.user.age <= 25)
        {
          cy.get('[id="ageRange"]').select('أقل من 25 سنة')
        }
        else if(this.user.age <= 35)
        {
          cy.get('[id="ageRange"]').select('من 25 الي 35 سنة')
        }
        else if(this.user.age <= 45)
        {
          cy.get('[id="ageRange"]').select('من 35 الي 45 سنة')
        }
        else if(this.user.age <= 55)
        {
          cy.get('[id="ageRange"]').select('من 45 الي 55 سنة')
        }
        else
        {
          cy.get('[id="ageRange"]').select('أكثر من 55 سنة')
        }

        //accept terms and conditions
        cy.get('[id="termsAndConditionsCheck"]').check({ force: true }).should('be.checked')

        //Here I can't test the recaptcha, so I give time to do it manually
        cy.wait(30000)

        //click on the register button
        cy.get('[type="submit"]').click()



        //Here, Registration should be completed

      })

    
  })